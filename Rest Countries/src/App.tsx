import { createContext, useState } from "react";

import Navbar from "./components/Navbar";
import "./App.css";
import { BrowserRouter as Router,Routes,Route } from 'react-router-dom';
import NotFound from "./components/404";
import Home from './components/home';
import CardDetail from "./components/CardDetail";

export const darkmodecontext = createContext(false);


function App() {
 
  function togglemode() {
    setdarkmode(darkmode ? false : true);
  }

  const [darkmode, setdarkmode] = useState(false);
    
  return (
    <darkmodecontext.Provider value={darkmode}>
      <Navbar toggletheme={togglemode}></Navbar>
   
    <Router>
      <Routes>
        <Route path="/" element={<Home  darkmode={darkmode} />} />
        <Route path="/country/:id" element={<CardDetail/>}  ></Route>
        <Route path="*" element={ <NotFound/>}  ></Route>
      </Routes>
    </Router>   
    
      
        
        {/* <Home darkmode={darkmode}></Home> */}


      </darkmodecontext.Provider>
    
  );
}

export default App;
