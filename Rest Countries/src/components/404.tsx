function NotFound() {
    return (  
        <h1 className="text-warning"> 404 Page Not Found</h1>
    );
}

export default NotFound;