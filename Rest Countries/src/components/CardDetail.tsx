import { useContext, useEffect, useState } from 'react';
import { darkmodecontext } from '../App';
import './CardDetail.css'
import { useNavigate, useParams } from 'react-router-dom';
let datatype: {  [key: string]: any; }
 
function CardDetail() {
   
  let darkmode= useContext(darkmodecontext)
  let navigator= useNavigate()
  const {id}  =  useParams()
  const [status,setstatus ]=useState('Loading...')   
  const [data,setdata]=useState(datatype);

 

 useEffect(()=>{
    let url= `https://restcountries.com/v3.1/alpha/${id}?fields=name,capital,currencies,borders,subregion,tld,languages,region,population,flags`;
          
     setstatus('Loading...')
          
     fetch(url).then((res) => {

             //console.log(res)
              if(res.status!==200)
             {
              throw res;
             }
             
             return res.json();

            }).then(
              (data) => {
              //  console.log(data);
               setstatus("");
               setdata(data);

             }).catch( (err)=>{
              
              setstatus(err.statusText);
              
       });
      },[id])
     

 function NavigateToBorderCountries(id:string)
 {
    console.log(id);
    navigator(`/country/${id}`)
 }

    
    return ( <>
    { 
      status?
    <h1 className="text-warning">{status}</h1>:   
    
    <div className={`button ${darkmode?'dark ':''} `}>
      
    <button type="button"  onClick={()=>{navigator('/')}} className={`btn back mx-5 mt-5 px-4 ${darkmode?'dark ':'btn-light'} fs-5`} > <i className="fa-solid fa-arrow-left"></i> Back</button>

    <div className={`container-fluid  ${darkmode?'dark':''} cont-detail p-5 my-2 d-flex`}>
   
        <div className="left">
        <img className='flag-img my-2 ' src={data.flags.svg}/>
        </div>
    
    <div className="outerright d-flex flex-column  flex-grow-1">
    <h5 className=' w-75 fs-1 '>{data.name.common}</h5>
     <div className="right d-flex flex-grow-1">
        <div className="inerRight  w-50  ">
        
      <p className="p-0 m-0 fw-medium"> Native Name: <span className='fw-light'>{data.name.common}</span></p>
      <p className="p-0 m-0 fw-medium">Population: <span className='fw-light'> {data.population.toLocaleString()} </span> </p>
      <p className="p-0 m-0 fw-medium">Region:  <span className='fw-light'>{data.region} </span></p>
      <p className="p-0 m-0 fw-medium">Subregion:  <span className='fw-light'>{data.subregion}</span></p>
      <p className="p-0 m-0 fw-medium">Capital:<span className='fw-light'> {data.capital}</span> </p>
     
     </div>
     
     <div className="innerleft justify-content-center ">
      
       <p className="p-0 m-0 fw-medium">Top Level Domain:  <span className='fw-light'> {data.tld}</span></p>
       <p className="p-0 m-0 fw-medium">Currencies:   <span className='fw-light'>{ data.currencies[Object.keys(data.currencies)[0]]? data.currencies[Object.keys(data.currencies)[0]].name:`` } </span></p>
       <p className="p-0 m-0 fw-medium">Languages: <span className='fw-light'>{Object.values(data.languages).join(',')}</span> </p>
     </div>
     
        </div>

       <p className="p-0  fw-medium border-c">Border Countries: </p>
       <div className="d-flex flex-wrap  b-cont"> 
        {  
         data.borders.map((ele:string)=>{

          return <button type="button" onClick={()=>{NavigateToBorderCountries(ele)}} key={ele} className={`btn bder btn-outline-secondary mx-1 ${darkmode?'dark ':''}  py-1 px-4 `}>  {ele} </button> 

        })}
        </div>
      </div>
    </div>    
 </div>
}
    </> );
}

export default CardDetail;