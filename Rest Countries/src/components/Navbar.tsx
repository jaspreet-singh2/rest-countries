import './navbar.css'
import {darkmodecontext } from '../App';
import { useContext } from 'react';
function navbar({toggletheme}:any) {
    
   let darkmodevalue= useContext(darkmodecontext)
    
    return ( 
      
      <nav className={`navbar navbars ${darkmodevalue?'dark':''} p-3 px-4 justify-content-between`}>
      
         <style>{`body { background-color: ${darkmodevalue?'hsl(207, 26%, 17%)':"hsl(0, 0%, 100%)"} ; }`}</style>
      
      <h1 className=''>Where in the world?</h1>
         
         <button onClick={toggletheme} className={`btn ${darkmodevalue?'btn-dark':""} p-3 my-2 my-sm-0` }>{darkmodevalue?'Light Mode':'Dark Mode'}</button>
      
      </nav>


 

     );

}

export default navbar;