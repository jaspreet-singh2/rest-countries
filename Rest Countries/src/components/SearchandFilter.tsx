import { useContext } from "react";
import './SearchandFilter.css';
import {darkmodecontext} from '../App'

let search='';
let region ='ALL';
let subregionv='ALL';
let shortBy="none";
let orderby="ASC";

function SearchandFilter({filterandsearch,subregion}:any){

  let darkmodevalue=useContext( darkmodecontext)
  
 

  return (
  <div className={`container-fluid p-5  ${darkmodevalue?'dark':''}`}>
  <div className={`container-fluid  filter-flex ${darkmodevalue?'dark':''} search d-flex flex-wrap  justify-content-between   `}>
    

    <div className="form-group has-search">
    <span className="fa fa-search form-control-feedback"></span>
    <input onChange={(eve)=>{search =eve.target.value ;  filterandsearch(search,region,subregionv,shortBy,orderby);}} type="text" className={` ${darkmodevalue?'form-control-dark':''}  form-control`} placeholder="Search for Country"/>
    </div>
    
     
 <select onChange={(eve)=>{ region=eve.target.value;  filterandsearch(search,region,'ALL',shortBy,orderby);} }className={`form-select  ${darkmodevalue?'dark':''}   form-select-1  max-content`} >
  <option defaultValue='ALL'>ALL</option>
  <option value="Africa">Africa</option> 
  <option value="Americas">Americas</option>
  <option value="Asia">Asia</option>
  <option value="Europe">Europe</option>
  <option value="Oceania">Oceania</option>
  </select>


<select onChange={(eve)=>{ subregionv=eve.target.value;   filterandsearch(search,region,subregionv,shortBy,orderby);} } className={` ${darkmodevalue?'dark':''} col-2 form-select max-content `} >
  <option   defaultValue='ALL'>Filter BY Subregion</option>
    {
      [...subregion].map((element: string) => {
        return  <option  key ={element} value={element}>{element}</option>
      })
    }
</select>


<select onChange={(eve)=>{ shortBy=eve.target.value;  filterandsearch(search,region,subregionv,shortBy,orderby);} } className={`${darkmodevalue?'dark':''}  form-select max-content`}>
  <option defaultValue='none'>Short BY</option>
  <option value="Area">Area</option>
  <option value="Population">Population</option>
</select>

<select onChange={(eve)=>{ orderby=eve.target.value;  filterandsearch(search,region,subregionv,shortBy,orderby);} } className={` form-select max-content ${darkmodevalue?'dark':''}`}>
<option defaultValue='ASC'>ASC</option>
<option value="DESC">DESC</option>
</select>



    </div>
  </div>
 
 );

}

export default SearchandFilter;
