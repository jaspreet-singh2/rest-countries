 import './card.css'
 import {darkmodecontext} from '../App'
import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';

function CountryCard({country} :any) {
    let navigator=useNavigate()
    let darkmodevalue=useContext(darkmodecontext);

    return ( 

 <div className={`container card ${darkmodevalue?'dark' :''} my-3 m-0 pb-4 px-0 `  } onClick={()=>{navigator(`/country/${country.cca3}`) }}>
   
    <img src={country.flags.svg}  className="Cimg mb-3  rounded"/>
   
    <div className="details px-4">

     <h5 className='fw-bold w-75'>{country.name.common}</h5>
     <p className="p-0 m-0 fw-medium"> Population: <span className='fw-light'>{country.population.toLocaleString()}</span></p>
     <p className="p-0 m-0 fw-medium">Region:  <span className='fw-light'>{country.region} </span></p>
     <p className="p-0 m-0 fw-medium">Subregion:  <span className='fw-light'>{country.subregion} </span></p>
     <p className="p-0 m-0 fw-medium">Capital:<span className='fw-light'> {country.capital}</span> </p>
     <p className="p-0 m-0 fw-medium">Area:<span className='fw-light'> {country.area.toLocaleString()} sqm</span> </p>

    </div>

 </div>
 );
} 

export default CountryCard;