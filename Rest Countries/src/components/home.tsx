
import { useState,useEffect,useRef } from "react";
import SearchandFilter from "./SearchandFilter";
import CountryCard from "./card";


let subregion: {[key: string]: Set<string>;}={ALL:new Set([])};


function Home({darkmode}:any) {

const rest: any[] = [];
const cachedata: any[] = [];
const [region,setregion]=useState('ALL');
const constdata = useRef(cachedata);
const [arr, setarr] = useState(rest);
const [status, setstatus] = useState("Loading...");

function CreateSubRegions()
{
   
 constdata.current.forEach(element => {
           
 if(subregion[element.region]==undefined)
 {
   subregion[element.region]=new Set<string>;
 }
   subregion[element.region].add(element.subregion);

 });

} 

 function filterandsearch(name: string, region: any,subregion:any,shortBy:any,orderby:any) {
  
  console.log(name, region,subregion);
  
  let boolregion = true;
  let bool =true
 
 
  if (region == "ALL") {
     boolregion = false;
   }
 
   if(subregion =="ALL")
   {
     bool=false;
   }
  
   let arr2 = constdata.current.filter((ele) => {
     if (
       (boolregion ? ele.region == region : true) && ele.name.common.toLowerCase().includes(name.toLowerCase() )&& (bool ? ele.subregion == subregion : true)
     ) {
       return ele;
     }
   });


 switch(shortBy){
  case 'Area':
   arr2.sort((a:any,b:any)=>{ return orderby=='DESC'?(b.area - a.area):-(b.area - a.area) })
   break;
   case 'Population':
   arr2.sort((a:any,b:any) =>{ return orderby=='DESC'?(b.population - a.population):-(b.population - a.population) })    
   break;
 }

   setregion(region);
   setarr(arr2);
 
  }




 useEffect(() => {
   fetch(
     `https://restcountries.com/v3.1/all?fields=name,capital,flags,population,region,subregion,area,cca3`
   )
     .then((res) => res.json())
     .then((data) => {
       constdata.current = data;
       setarr(data);
       setstatus("");
       CreateSubRegions();
     })
     .catch((error) => {
       setstatus(error.message);
    
     });
 }, []);




    return ( 
        <> 
    <SearchandFilter  filterandsearch={filterandsearch} subregion={subregion[region]}   />
    {
    (constdata.current.length != 0 && arr.length === 0) || status ? (
       
       <div  className={`w-100 h-75 ${darkmode ? "dark" : ""} d-flex `}>
        
        <h1 className="text-warning mx-auto d-inline">
          { 
          constdata.current.length != 0 && arr.length === 0 ? "No Such Country Found": status
          }
        </h1>

       </div>

    ) : (

      <div className={`cards d-flex ${ darkmode ? "dark" : ""} gy-5 flex-wrap p-5 justify-content-between`}>
        {
          
        arr.map((country) => {
          return (
            <CountryCard key={country.cca3}   country={country} />
          );
        })
        }

      </div>

    )}
    </>
);
}

export default Home;